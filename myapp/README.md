PROYECTO BICIS
hola
Comenzando 🚀
Por favor no olvides acceder al directorio "myapp" porque por defecto me estaba a una carpeta antes
simplemente digita el comando en tu terminal cd myapp

Pre-requisitos 📋
Una vez estés seguro que estás en el directorio correcto de app, por favor no olvides el npm install para 
instalar todas las dependencia que usa esta APP.

Da un ejemplo
Instalación 🔧
    Abre mi proyecto en Visual Studio Code o en tu editor fav.
    Para abrir la terminal digita Ctrl+Shift+ñ
    Desde un terminal de tu Visual Studio Code debes acceder a mi carpeta
    si has abierto la carpeta antes y luego el terminal por defecto aparecerás en:
    La siguiente línea es un ejemplo: 
    "C:\Users\Lenovo\Desktop\proyecto_bicis"
    pero debes acceder a myapp en tu terminal colocas cd myapp
    "C:\Users\Lenovo\Desktop\proyecto_bicis\myapp"
    una vez estando ahí por favor ingresa el siguiente comando npm install
    Ejemplo
    C:\Users\Lenovo\Desktop\proyecto_bicis\myapp> npm install
    Esto instalará todas las dependencias
    
    Una vez instalado todo por favor ejecuta el comando
    npm run devstart para iniciar con el localhost
    el puerto del localhost es el 3000
    por defecto aparecerás en el mensaje de Express
    Para acceder a la página de bicis entra al siguiente
    http://localhost:3000/bicicletas
    Por favor ejecutar el npm test

Construido con 🛠️
    Visual Studio Code
    Postman - para el uso de la api
    LeafLets - para mapas


Versionado 📌
    versión 1

Autores ✒️
    Abel Calderóm Peña 


Licencia 📄
Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo LICENSE.md para detalles


Muchas gracias por el calificar mi app. 
