var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testeando users',function(){
    beforeEach(function(done){
        mongoDb = 'mongodb://localhost/testDb';
        mongoose.connect(mongoDb,{useNewUrlParser: true,useCreateIndex:true,useUnifiedTopology:true});
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'Conexión Fallida'));
        db.once('open',function(){
            console.log('Me acabo de conectar a la base de datos de prueba');
            done();
        });
    });
    afterEach(function(done){
        //Borrado en cascada, dado por el uso de los callbacks xd 
        Reserva.deleteMany({},function(err,success){
            if(err) console.log(err);
            Usuario.deleteMany({},function(err,success){
                if(err) console.log(err);
                Bicicleta.deleteMany({},function(err,success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });
    describe('agregando una reserva ...',()=>{
        it('reservando una bici',(done)=>{
            const usuario = new Usuario({nombre:'Abel'});
            usuario.save();
            const bicicleta  = new Bicicleta({code:1,color:'verde',modelo:'urbana',ubicacion:[-20,-50]});
            bicicleta.save();
            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id,hoy,mañana,function(err,reserva){
                /*Reserva.find({},function(err,bicis){
                    Usuario.allUsers(function(err,users){
                        expect(users.length).toBe(1);
                        console.log(users[0]);
                        done();
                    });
                });*/
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    //Debería ser uno pero en el código de reserva al solicitar la cantidad de días al final le pide la
                    //la diferencia aumentada en uno
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    console.log('dias de reserva'+reservas[0].diasDeReserva());
                    expect(reservas[0].bicicleta.code).toBe(bicicleta.code);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
         });
    });
    describe('total de usuarios debe ser 0 ',function(){
        it('verificando el total de usuarios',function(done){
            Usuario.allUsers(function(err,users){
                expect(users.length).toBe(0);
                done();
            });
        });      
    });
});