/*Documento después de que el mongo fue vinculado a la bicicleta*/
var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');

describe('Test del model Bicicleta',function(){
    //Ejecucion antes de cada test
    //done para lidiar con el asincronismo
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testDb';
        mongoose.connect(mongoDB,{useNewUrlParser: true,useCreateIndex:true,useUnifiedTopology:true});
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'Conexión Fallida'));
        db.once('open',function(){
            console.log('Me acabo de conectar a la base de datos de prueba');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({},function(error,succes){
            if(error) console.log(error);
            done();
        })
    });
    //Testeo para la creación de una instancia(no es lo mismo que guardar en una database)
    describe('Creando una instancia de bicicleta',function(){
        //Aqui no se requiere de un  parametro done porque esto no es asincronico o sea tal y como están declaradas las líneas así funcionará
        it('Creando instancia...',function(){
            var bici = Bicicleta.createInstance(1,'rojo','montañera',[-50,-20]);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('montañera');
            expect(bici.code).toEqual(1);
            expect(bici.ubicacion[0]).toEqual(-50);
            expect(bici.ubicacion[1]).toEqual(-20);
            console.log(bici);
        });
    });
    //Test del modelo bicicleta
    describe('Test para ALL BICIS',function(){
        it('Verificar que el total de bicicletas sea 0', function(done){
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        });
    });
    describe('Bicicleta.add para comprobar que estamos agregando un documento al mongodb',function(){
        it('Verificando que se está agregando un documento',(done)=>{
            //var Bici = Bicicleta.createInstance(999,'verde','callejera',[-20,-50]);
            var Bici  = new Bicicleta({code:99,color:'verde',modelo:'callejera',ubicacion:[-20,-50]});
            Bicicleta.add(Bici,function(err,bic){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(Bici.code);
                    done();
                });
            });
        });
    });
    describe('Bicicleta findbycode',function(){
        it('Buscando bicicleta por el codigo',function(done){
            var bici1 = new Bicicleta({code:99,color:'verde',modelo:'callejera',ubicacion:[-20,-50]});
            var bici2 = new Bicicleta({code:98,color:'rojo',modelo:'urbana',ubicacion:[-30,-35]});
            Bicicleta.add(bici1,function(err,bici){
                if(err)console.log(err);
                Bicicleta.add(bici2,function(err,bici){
                    if(err) console.log(err);
                    Bicicleta.findByCode(98,function(error,biciRecibida){
                        expect(biciRecibida.code).toEqual(bici2.code);
                        expect(biciRecibida.color).toBe(bici2.color);
                        done();
                    });
                });
            });
        });
    });
});

