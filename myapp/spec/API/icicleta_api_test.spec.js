/*Esto es la api ahora conectada con el mongoose */
/*Después de esto se agregó el modelo de usuario */
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', ()=>{
    beforeEach(function(done){
        var mongodb = 'mongodb://localhost/newTestDB';
        mongoose.connect(mongodb,{useNewUrlParser: true,useCreateIndex:true,useUnifiedTopology:true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',function(){
            console.log('we are connected to test database :D ');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err){
                console.log(err);
            }
            done();
        });
    });

    describe('GET BICICLETA / ',()=>{
        it('status 200',(done)=>{
            request.get('http://localhost:3000/api/bicicletas/lista',function(error,response,body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.Bicicletas.length).toBe(0);
                console.log(result.Bicicletas);
                done();
            });
        });
    });
    describe('POST BICICLETAS /create',()=>{
        it('status 200', (done)=>{
            var headers = {'content-type': 'application/json'};
            var aBici = '{ "id":10,"color":"rojo","modelo":"urbana","lat":-34,"lng":-54}';
            request.post({
                headers:headers,
                url:base_url+'/crear',
                body:aBici
            }, function(error,response,body){
                if(expect(response.statusCode).toBe(200)){
                    console.log("Pasé");
                }
                var Bici = JSON.parse(body).Bicicleta;
                console.log(Bici);
                expect(Bici.color).toBe('rojo');
                expect(Bici.ubicacion[0]).toBe(-34);
                expect(Bici.ubicacion[1]).toBe(-54);
                done();
            })
        });
    });
});