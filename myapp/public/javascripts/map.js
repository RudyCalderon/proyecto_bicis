var mymap = L.map('mapid').setView([-36.849284, 174.765056], 13);
var icono = L.icon({
    iconUrl: '../assets/img/bike.png',

    iconSize:     [60, 60],
    iconAnchor:   [30, 60]
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution:'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

$.ajax({
    dataType : 'json',
    url : 'api/bicicletas/lista',
    success: function (result){
        console.log(result);
        result.Bicicletas.forEach(function (bici){
            L.marker(bici.ubicacion, {title:bici.id,icon: icono}).addTo(mymap).bindPopup('Modelo: '+bici.modelo+' Color:'+bici.color);
        })
    }
})
