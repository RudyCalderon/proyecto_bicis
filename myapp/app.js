require('dotenv').config();
require('newrelic');

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var biciRouter = require('./routes/bicicleta');
var biciApi = require('./routes/API/bicicletaAPI');
var userApi = require('./routes/API/usuarioApi');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/API/auth');
var Token = require('./models/token');
//Importando el passport
const passport = require('./config/passport');
//Importando el session
const session = require('express-session');
const MongoDBStore  =require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');
var app = express();
let store;
if(process.env.NODE_ENV ==='development'){
  store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection:'sessions'
  });
  store.on('error',function(error){
    assert.ifError(err);
    assert.ok(false);
  });
}
app.set('secretKey','jwt_pwd_!!223344');
app.use(session({
  cookie:{maxAge:240*60*60*1000},
  store:store,
  saveUninitialized:true,
  resave:'true',
  secret:'red_bicis!!!i**!!----!___!_-____------_!'
}));
//Inicializar passport
app.use(passport.initialize());
app.use(passport.session());
//Fin inicializar passport

//Conexión a la Base de Datos
//String de Conexión
var mongoDB = process.env.MONGO_URI;
//Variable Mongoose conectando.
mongoose.connect(mongoDB,{useNewUrlParser:true,useCreateIndex:true,useUnifiedTopology:true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console,'Conexión a la base de datos fallida :(.'));
//End Conecction to Database

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/***************************************************************************/
app.get('/auth/google',passport.authenticate('google',{
    scope:[
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read',
      'profile',
      'email'
    ]
  })
);
/*app.get('/auth/google',passport.authenticate('google',{scope:['https://www.googleapis.com/auth/plus.login','https://www.googleapis.com/auth/plus.profile.emails.read']});*/
app.get('/auth/google/callback',passport.authenticate('google',{
  successRedirect:'/bicicletas',
  failureRedirect:'/error'
}));
//Rutas del login 
app.get('/login',function(req,res){
  res.render('session/login');
});
app.post('/login',function(req,res,next){
  //passport
  passport.authenticate('local',function(err,usuario,info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{info});
    req.login(usuario,function(err){
      if(err) return next(err);
      return res.redirect('/bicicletas');
    });

  })(req,res,next) ;
});
app.get('/logout',function(req,res){
  req.logout();
  res.render('session/login',{finSe:'Vuelve pronto por favor :D!'})
  //res.redirect('login');
})
app.get('/forgotPassword',function(req,res){
  res.render('session/forgotPassword');
});
var Usuario = require('./models/usuario');
const { decode } = require('punycode');
const { assert } = require('console');
app.post('/forgotPassword',function(req,res){
  Usuario.findOne({email:req.body.email},function(err,usuario){
   if(!usuario) return res.render('session/forgotPassword',{info:{message:'No existe dado email registrado'}});
   usuario.resetPassword(function(err){
     if(err) return next(err);
     console.log('session/forgotPasswordMessage');
   });
   res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function(req, res, next){
  Token.findOne({ token: req.params.token }, function(err, token){
    if(!token) return res.status(400).send({ msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado'});
    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  });
});

app.post('/resetPassword', function(req, res){
  if(req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, usuario: new Usuario({email: req.body.email})});
    return;
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    usuario.password = req.body.password;
    usuario.save(function(err){
      if(err){
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      }else{
        res.redirect('/login');
      }
    });
  });
});

/***************************************************************************/
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas',loggedIn,biciRouter);
app.use('/usuarios',usuariosRouter);
app.use('/token',tokenRouter);
app.use('/api/bicicletas',validarUsuario,biciApi);
app.use('/api/usuarios',userApi);
app.use('/api/auth',authApiRouter);
app.use('/privacy_policy',function(req,res){
  res.sendFile('public/privacy_policy.html');
});
app.use('/googleca9cece87dd8d4ba',function(req,res){
  res.sendFile('public/googleca9cece87dd8d4ba.html');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
//Logged
function loggedIn(req,res,next){
	if(req.user){
		next();
	}else{
		console.log('user sin loguearse');
		res.redirect('/login');
	}
}
function validarUsuario(req,res,next){
  jwt.verify(req.headers['x-access-token'],req.app.get('secretKey'),function(err,decoded){
    if(err){
      res.json({status:'error',message:err.message,data:null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify: '+ decoded);
      next();
    }
  });
}
module.exports = app;
