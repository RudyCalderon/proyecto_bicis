var Bicicleta = require('../../models/bicicleta');


exports.lista_Bicicletas = function (req,res){
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({Bicicletas:bicis});
    })
}
exports.crear_Bicicleta = function (req,res){
    var bici = Bicicleta.createInstance(req.body.code,req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici,function(err,nbici){
        res.status(200).json({Bicicleta:nbici})
    });
}
exports.actualizar_Bicicleta = function(req,res){
    Bicicleta.updateByCode(req.params.code,req.body.code,req.body.color,req.body.modelo,req.body.lat,req.body.lng,function(error,doc){
        if(error){
            res.status(500).send();
        }else{
            res.status(200).send();
        }
    });
}
exports.borrar_Bicicleta = function (req,res){
    Bicicleta.removeByCode(req.params.code,function(err,bici){
        res.status(204).send();
    })
}