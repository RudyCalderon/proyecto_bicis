var Bicicleta = require('../models/bicicleta');

exports.index = function (req,res){
    res.render('bicicletas/index',{user:req.user});
}
exports.bici_List = function (req,res){
    Bicicleta.allBicis(function(err,bicis){
        res.render('bicicletas/lista',{bicis: bicis});
    });
}
exports.bici_Crear_get = function (req,res){
    res.render('bicicletas/crear');
}
exports.bici_Crear_post = function (req,res){
    var ub = [];
    ub=[req.body.latitud,req.body.longitud];
    var bici = Bicicleta.createInstance(req.body.code,req.body.color,req.body.modelo,ub);
    Bicicleta.add(bici);
    res.redirect('lista');
}
exports.bici_Actualizar_get = function (req,res){
    /*var bici  = Bicicleta.buscarPorID(req.params.id);
    res.render('bicicletas/actualizar',{bike: bici});*/
    Bicicleta.findById(req.params.id,function(err,bici){
        res.render('bicicletas/actualizar',{bike:bici});
    });
}
exports.bici_Actualizar_post = function (req,res){
    /*var bici = Bicicleta.buscarPorID(req.params.id);
    bici.id = req.body.id
    bici.color = req.body.color
    bici.modelo = req.body.modelo
    bici.ubicacion = [req.body.latitud,req.body.longitud];
    res.redirect('/bicicletas/lista');*/
    Bicicleta.updateById(req.params.id,req.body.code,req.body.color,req.body.modelo,req.body.latitud,req.body.longitud,function (err,succ){
        res.redirect('/bicicletas/lista');
    });
    
}
exports.bici_Delete_post = function(req,res){
    /*Bicicleta.eliminarPorID(req.params.id);

    res.redirect('/bicicletas/lista');*/
    Bicicleta.removeById(req.params.id,function(err,succ){
        res.redirect('/bicicletas/lista');
    });
}