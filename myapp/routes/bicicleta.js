var express = require('express');
var router = express.Router();
var biciController = require('../controllers/bicicleta');

router.get('/',biciController.index);

router.get('/lista',biciController.bici_List);

router.get('/crear',biciController.bici_Crear_get);
router.post('/crear',biciController.bici_Crear_post);

router.get('/:id/actualizar',biciController.bici_Actualizar_get);
router.post('/:id/actualizar',biciController.bici_Actualizar_post);

router.post('/:id/borrar',biciController.bici_Delete_post);

module.exports = router;