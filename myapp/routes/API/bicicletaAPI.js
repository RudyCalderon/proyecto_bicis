var express = require('express');
var router = express.Router();
var biciApi = require('../../controllers/API/bicicletaAPI');

router.get('/lista',biciApi.lista_Bicicletas);

router.post('/crear',biciApi.crear_Bicicleta);

router.put('/:code/actualizar',biciApi.actualizar_Bicicleta);

router.delete('/:code/eliminar',biciApi.borrar_Bicicleta);

module.exports = router;