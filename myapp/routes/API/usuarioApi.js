var express = require('express');
var router = express.Router();
var usuarioController = require('../../controllers/API/usuarioApi');

router.get('/lista',usuarioController.usuarios_list);
router.post('/crear',usuarioController.usuarios_create);
router.post('/reservar',usuarioController.usuario_reservar);

module.exports = router;