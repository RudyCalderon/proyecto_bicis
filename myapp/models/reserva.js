var moment  = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const Usuario = require('./usuario');

var reservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    //es un id de un schema xd
    bicicleta: {type:mongoose.Schema.Types.ObjectId,ref:'Bicicleta'},
    usuario: {type:mongoose.Schema.Types.ObjectId, ref:'Usuario'}
});

reservaSchema.methods.diasDeReserva = function(){
    //Esto nos muestra cuantos días está reservada la bicicleta 
    return moment(this.hasta).diff(moment(this.desde),'days') +1;
}

module.exports = mongoose.model('Reserva',reservaSchema);