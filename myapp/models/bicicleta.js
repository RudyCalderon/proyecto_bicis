/*Bicicleta de Objeto a Schema de MongoDB */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//Creando nuestro Schema para la base de datos.
var bicicletaSchema = new Schema({
    code: Number,
    color:String,
    modelo:String,
    ubicacion : {
        type: [Number],
        index: {type:'2dsphere',sparse:true} 
    }
});
bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new this({
        code:code,
        color:color,
        modelo:modelo,
        ubicacion:ubicacion
    });
}
bicicletaSchema.methods.toString = function () {
    return 'code:' + this.code  + ' | color: '+ this.color;
}
bicicletaSchema.statics.allBicis = function (callback){
     return this.find({},callback);
}
bicicletaSchema.statics.findById = function(id,callback){
    //Callbacks para lidiar con el asincronismo
    return this.findOne({_id:id},callback);
}
bicicletaSchema.statics.findByCode = function(aCode,callback){
    //Callbacks para lidiar con el asincronismo
    return this.findOne({code:aCode},callback);
}
bicicletaSchema.statics.add = function(bici,callback){
     this.create(bici,callback);
}
bicicletaSchema.statics.removeByCode = function(code,callback){
    this.deleteOne({code:code},callback);
}
bicicletaSchema.statics.removeById = function(id,callback){
    this.deleteOne({_id:id},callback);
}
bicicletaSchema.statics.updateById = function(id,nCode,nColor,nModel,nLat,nLng,cb){
    var consulta = {_id: id};
    this.update(consulta,{$set:{
            code:nCode,
            color:nColor,
            modelo:nModel,
            ubicacion:
               [nLat,nLng]
        }
    },cb);
}
bicicletaSchema.statics.updateByCode = function(aCode,nCode,nColor,nModel,nLat,nLng,cb){
    var consulta = {code: aCode};
    this.update(consulta,{$set:{
            code:nCode,
            color:nColor,
            modelo:nModel,
            ubicacion:
               [nLat,nLng]
        }
    },cb);
}
module.exports = mongoose.model('Bicicleta',bicicletaSchema);

