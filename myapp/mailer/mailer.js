var nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
//API KEY EN SENDGRID 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
let mailConfig;
if(process.env.NODE_ENV==='production'){
    const options = {
        auth:{
            api_key: process.env.SENDGRID_API_KEY
        }
    }
    mailConfig = sgTransport(options);
}else{
    if(process.env.NODE_ENV === 'staging'){
        console.log('XXXXXXXXXXXX');
        const options = {
            auth:{
                api_key:process.env.SENDGRID_API_KEY
            }
        }
        mailConfig = sgTransport(options);
    }else{
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_password
            }
        }
    }
}
/*const mailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'veronica.sauer7@ethereal.email',
        pass: 'v3aXyA4WR3RvNAEPfQ'
    }
}*/

module.exports = nodemailer.createTransport(mailConfig)